#import <stdio.h>
#import <stdlib.h>

#define LIMIT 1000
/**Find the sum of all the multiples of 3 or 5 below 1000.**/

int main(int argc, char const *argv[]) {
    int sum = 0;
    for (int i = 0; i < LIMIT; i++) {
        if (i%3 == 0 || i%5 == 0){
            sum = sum + i;
        }
    }

    printf("%s\n",sum);
    
    return 0;
}
